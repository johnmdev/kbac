#! /usr/bin/env python2
#
# kbac/kbac.py

# license--start
#
# Copyright (c) 2018, John Marshall. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of the author nor the
#       names of its contributors may be used to endorse or promote products
#       derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
# WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY
# DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
# (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
# ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
# SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# license--end

"""Key-based access control (KBAC) associates principals with a key.
Keys must be hashable and principals of arbitrary types which are
directly compared (i.e., not content compared). Key and principals
are application specific/defined. There is no fixed, canonical set
of keys or principals that work across all applications.

As an example, keys can be roles, services; principals can be
usernames, groupnames, uids, gids. A combination of different kinds
of principals can also be supported by using prefixes (e.g. string
for username, string with @ prefix for groupname).
"""

class KBAC:
    """KBAC object.

    <key>: [<principal>, ...]

    The KBAC can be initialized with a wildcard (e.g., "*") which
    can be used to support "match all". By default, no wildcard is
    set.
    """

    def __init__(self, wildcard=None):
        self.key2principals = {}
        self.wildcard = wildcard

    def add(self, key, principals):
        """Add principals to existing or empty list of principals
        associated with a key.
        """
        _principals = self.key2principals.setdefault(key, set([]))
        _principals.update(principals)

    def clear(self, key):
        """Clear principals setting for a key.
        """
        _principals = self.key2principals.get(key)
        if _principals:
            _principals.clear()

    def clear_all(self):
        """Clear all key-principals settings.
        """
        self.key2principals = {}

    def is_allowed(self, keys, principals):
        """Test if one of the given principals is allowed for one of
        the keys.
        """
        principals = set(principals)
        for key in keys:
            _principals = self.key2principals.get(key)
            if _principals:
                if self.wildcard and self.wildcard in _principals:
                    return True
                if principals.intersection(_principals):
                    return True
        return False

    def keys(self):
        """Return keys.
        """
        return self.key2principals.keys()

    def load(self, items):
        """Load rules from sequence of (k, v) items where k is the
        key and v is a CSV list of principals.
        """
        for key, v in items:
            principals = filter(None, [x.strip() for x in v.split(",")])
            self.add(key, principals)

    def principals(self, key):
        """Return principals associated with key.
        """
        return list(self.key2principals.get(key, []))

    def remove(self, key, principals):
        """Remove principals (if present) from those associated with
        a key.
        """
        _principals = self.key2principals.get(key)
        if _principals:
            _principals.difference_update(filter(None, principals))
