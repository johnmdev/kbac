# KBAC - Key-based Access Control

Key-based access control (KBAC) associates principals with a key.
Keys and principals are string-based and application
specific/defined. There is no fixed, canonical set of keys that
works across all applications. Principals may be anything that can
be represented as a string (e.g., username, groupname, uid, gid). A
combination of different kinds of principals can be supported by
using prefixes (e.g. @ for groupname).